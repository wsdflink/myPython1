__author__ = 'wsdflink'

from twisted.internet import protocol,reactor

HOST='localhost'
PORT=21567


class TSClntProtocol(protocol.Protocol):
    def sendData(self):
        data=raw_input('> ')
        if data:
            print '...sending data: %s' % data
            self.transport.write(data)
        else:
            self.transport.loseConnection()

    def connectionMade(self):
        self.sendData()

    def dataReceived(self, data):
        print data
        self.sendData()

# 1. derive
class TSClntFactory(protocol.ClientFactory):
    protocol=TSClntProtocol
    clientConnectionLost=clientConnectionFailed= lambda self, connector,reason: reactor.stop()

# 2. set protocol
# factory=protocol.ClientFactory()
# factory.protocol=TSClntProtocol
reactor.connectTCP(HOST,PORT,TSClntFactory())
# reactor.connectTCP(HOST,PORT,factory)
reactor.run()