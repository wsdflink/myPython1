__author__ = 'wsdflink'

import socket
from time import ctime

HOST = ''
PORT=21567
BUFSIZ= 1024
ADDR=(HOST,PORT)

tcpSerSock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
tcpSerSock.bind(ADDR)
tcpSerSock.listen(5)

while True:
    print 'wait for connections...'
    tcpCliSock,addr = tcpSerSock.accept()
    print 'connected from: ' + str(addr)

    while True:
        data= tcpCliSock.recv(BUFSIZ)
        print 'received: %s' % data
        if not data:
            break
        tcpCliSock.send('[%s] %s' % (ctime(),data))
    tcpCliSock.close()
tcpSerSock.close()